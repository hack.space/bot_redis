import os

BACKEND_URL = os.environ['BACKEND_URL']
DOCUMENT_API = os.environ['DOCUMENT_API']
