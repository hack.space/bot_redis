import os

# Redis DB connection parameters

REDIS_HOST = os.environ['REDIS_HOST']
REDIS_PORT = os.environ['REDIS_PORT']
REDIS_DB_ID = os.environ['REDIS_DB_ID']
REDIS_DB_ENCODING = os.environ['REDIS_DB_ENCODING']
