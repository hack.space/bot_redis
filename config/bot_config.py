import os

TOKEN = os.environ['TOKEN']
SIGNATURE_TOKEN = os.environ['SIG_TOKEN']
USER_AGREEMENT = 'https://gist.githubusercontent.com/Algruun/c393d6f1eef5edb212a07f584e7fe4a3/raw/' \
                 'a97b1e2e5da1111459a8ba2aa6c15e4aaf985640/user_agreement.txt' if 'USER_AGREEMENT' not in os.environ else \
    os.environ['USER_AGREEMENT']
