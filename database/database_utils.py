import redis

from config import redis_config


class UserDatabase:
    def __init__(self):
        self.db = redis.StrictRedis(redis_config.REDIS_HOST, redis_config.REDIS_PORT, redis_config.REDIS_DB_ID)

    def get_user(self, user_id):
        return {key.decode(redis_config.REDIS_DB_ENCODING): value.decode(redis_config.REDIS_DB_ENCODING)
                for key, value in self.db.hgetall(user_id).items()}

    def get_user_field(self, user_id, field_name):
        data = self.db.hget(user_id, field_name)
        if data:
            return data.decode(redis_config.REDIS_DB_ENCODING)
        else:
            return None

    def add_user(self, user_id, user_data):
        self.db.hmset(user_id, user_data)

    def set_user_field(self, user_id, field_name, field_value):
        return self.db.hset(user_id, field_name, field_value)
