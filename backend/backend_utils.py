import hashlib
import hmac
import json
from io import BytesIO

import requests
from requests.exceptions import RequestException

from config import bot_config
from config.backend_config import BACKEND_URL, DOCUMENT_API


def user_login(data):
    if 'photo_url' in data and data.get('photo_url'):
        response = requests.get(data.get('photo_url'))
        img = BytesIO(response.content)
        response = requests.post(DOCUMENT_API, files={'file': img})
        if response.status_code == 201:
            response = response.json()
            data.update({'photo_url': DOCUMENT_API + '/' + response.get('id')})
    data['hash'] = get_data_hash(data, bot_config.SIGNATURE_TOKEN)
    r = requests.post(BACKEND_URL + '/login/telegram?fields=hacker', json=data)
    if r.status_code != 200:
        raise RequestException(r.text)
    resp = r.json()
    return resp['token'], resp['hacker']


def get_skills(token):
    r = requests.get(BACKEND_URL + '/skills', headers={"Authorization": token, "Content-Type": 'application/json'})
    resp = r.json()
    return resp


def get_data_hash(data, bot_token):
    bot_token_key = hashlib.sha256(bytes(bot_token, encoding='utf-8')).digest()
    for key in data:
        if data[key] is None:
            data[key] = 'null'
    check_string = '\n'.join([f'{key}={data[key]}' for key in sorted(data.keys())])
    hmac_string = hmac.new(bot_token_key, msg=bytes(check_string, encoding='utf-8'),
                           digestmod=hashlib.sha256).hexdigest()
    return hmac_string


def get_current_user(token):
    r = requests.get(BACKEND_URL + '/hackers/me', headers={"Authorization": token, "Content-Type": 'application/json'})
    if r.status_code != 200:
        raise RequestException(r.text)
    resp = json.loads(r.text)
    return resp


def update_current_user(token, data):
    data.pop('id')
    data.pop('tgProfileLink')
    data.pop('stat')
    data_string = json.dumps(data)
    r = requests.put(BACKEND_URL + '/hackers/me', data=data_string,
                     headers={"Authorization": token, "Content-Type": 'application/json'})
    if r.status_code != 200:
        raise RequestException(r.text)


def get_user_events(token, status):
    r = requests.get(BACKEND_URL + f'/events?status={status}',
                     headers={"Authorization": token, "Content-Type": 'application/json'})
    if r.status_code not in [200]:
        raise RequestException(r.text)
    resp = r.json()
    return resp


def apply_for_event(event_id, token):
    r = requests.post(BACKEND_URL + f'/events/{event_id}/apply',
                      headers={"Authorization": token, "Content-Type": 'application/json'})
    if r.status_code not in [200, 409]:
        raise RequestException(r.text)
    return r.status_code


def get_participants(event_id, token):
    r = requests.get(BACKEND_URL + f'/hackers?eventId={event_id}',
                     headers={"Authorization": token, "Content-Type": 'application/json'})
    return r.json()


def participation_status_activate(token, event_id, password, location):
    password = password if password else 'null'
    if location:
        request_data = {"enteringWord": password,
                        "location": {'lat': location['latitude'], 'lng': location['longitude']}}
    else:
        request_data = {"enteringWord": password}

    r = requests.post(BACKEND_URL + f'/events/{event_id}/activate',
                      data=json.dumps(request_data),
                      headers={"Authorization": token, "Content-Type": 'application/json'})
    if r.status_code != 200:
        raise RequestException(r.text)
    resp = r.json()
    return resp['status']


def participation_status_finish(event_id, token):
    r = requests.post(BACKEND_URL + f'/events/{event_id}/finish',
                      data=json.dumps({}),
                      headers={"Authorization": token, "Content-Type": 'application/json'})
    if r.status_code != 200:
        raise RequestException(r.text)
    resp = r.json()
    return resp['status']


def participation_status_revert(event_id, token):
    r = requests.post(BACKEND_URL + f'/events/{event_id}/activate',
                      data=json.dumps({}),
                      headers={"Authorization": token, "Content-Type": 'application/json'})
    if r.status_code != 200:
        raise RequestException(r.text)
    resp = r.json()
    return resp['status']


def toggle_searchable(event_id, token):
    r = requests.post(BACKEND_URL + f'/events/{event_id}/toggle_is_searchable',
                      data=json.dumps({}),
                      headers={"Authorization": token, "Content-Type": 'application/json'})
    resp = r.json()
    return resp['isSearchable']


def get_event(event_id, token):
    r = requests.get(BACKEND_URL + f'/events/{event_id}',
                     headers={"Authorization": token, "Content-Type": 'application/json'})

    resp = r.json()
    return resp


def verify_email(email, code, token):
    r = requests.post(BACKEND_URL + f'/hackers/email-verification',
                      data=json.dumps({"email": email, "code": str(code)}),
                      headers={"Authorization": token, "Content-Type": 'application/json'})

    resp = r.status_code
    return resp


def resend_code(email, token):
    r = requests.post(BACKEND_URL + f'/hackers/resend-confirm-code',
                      data=json.dumps({"email": email}),
                      headers={"Authorization": token, "Content-Type": 'application/json'})

    resp = r.status_code
    try_after = None
    if 'Retry-After' in r.headers:
        try_after = int(r.headers['Retry-After']) // 1000
    return resp, try_after
