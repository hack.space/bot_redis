import gettext
import os


def set_language(language: str):
    if language == 'Russian':
        base_dir = os.path.dirname(os.path.abspath(__file__))
        home_dir = '/'.join(base_dir.split('/')[:-1])
        # os.path.join(home_dir, 'states', 'state_view_utils', 'locale')
        ru = gettext.translation('base', os.path.join(home_dir, 'states', 'state_view_utils', 'locale'),
                                 languages=["ru"])
        ru.install()
        return ru.gettext
    else:
        return gettext.gettext
