import json
from states.state_meta import StateMeta


class NoStateFoundException(Exception):
    pass


class BasicState(metaclass=StateMeta):
    tag = 'BASIC'
    state_data = {}

    @staticmethod
    def create_transition(new_state_tag, **kwargs):
        if new_state_tag not in StateMeta.states:
            raise NoStateFoundException
        return new_state_tag, kwargs

    def __init__(self, user):
        self.user = user

    def enter(self, **kwargs):
        pass

    def silent_enter(self, **kwargs):
        self.enter(**kwargs)

    def update(self, bot, update):
        pass

    def update_inline_req(self, bot, update):
        pass

    def update_inline_ans(self, bot, update):
        pass

    def update_callback(self, bot, update):
        pass

    def save_state(self):
        state_data_string = json.dumps(self.state_data)
        self.user.users_db.set_user_field(self.user.user_id, 'state_data', state_data_string)

    def exit(self):
        pass
