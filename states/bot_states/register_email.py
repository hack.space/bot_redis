import re

from backend import backend_utils
from states.basic_state import BasicState
from states.bot_states import tags
from states.state_view_utils.register_email_view import draw_email_request, draw_returning_email_request, \
    draw_email_error, draw_email_set


class RegisterEmailState(BasicState):
    tag = 'REGISTER_EMAIL'
    email_regex = r"([a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+)"

    def __init__(self, user):
        # super(RegisterEmailState, self).__init__(user)
        self.user = user
        self.language = self.user.users_db.get_user_field(self.user.user_id, 'language')

    def enter(self, returning=False):
        self.language = self.user.users_db.get_user_field(self.user.user_id, 'language')
        if not returning:
            draw_email_request(self.user, self.language)
        else:
            draw_returning_email_request(self.user, self.language)

    def silent_enter(self, **kwargs):
        BasicState.enter(self, **kwargs)

    def update(self, bot, update):
        reply = update.message.text
        email_match = re.search(self.email_regex, reply)
        if not email_match:
            draw_email_error(self.user, self.language)
        else:
            email = email_match[0]

            user_data = backend_utils.get_current_user(self.user.token)
            user_data['email'] = email
            backend_utils.update_current_user(self.user.token, user_data)
            draw_email_set(self.user, email, self.language)

            return BasicState.create_transition(tags.REGISTER_EMAIL_CONFIRMATION_CODE, email=email)

    def update_inline_req(self, bot, update):
        pass

    def update_inline_ans(self, bot, update):
        pass

    def update_callback(self, bot, update):
        pass

    def exit(self):
        pass
