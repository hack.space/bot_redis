from states.basic_state import BasicState
from states.bot_states import tags
from states.state_view_utils.hackathon_choice_menu_view import draw_event_choice_keyboard, draw_no_such_event_error
from backend.language_utils import set_language

class HackathonChoiceMenuState(BasicState):
    tag = 'HACKATHON_CHOICE_MENU'

    def __init__(self, user):
        # super(HackathonChoiceMenuState, self).__init__(user)
        self.event_dict: dict = {}
        self.user = user
        self.language = self.user.users_db.get_user_field(self.user.user_id, 'language')

    def enter(self, events):
        self.event_dict = {event['title']: event['id'] for event in events}
        self.state_data = {'events': self.event_dict}

        self.language = self.user.users_db.get_user_field(self.user.user_id, 'language')
        draw_event_choice_keyboard(self.user, self.event_dict, self.language)

    def silent_enter(self, state_data):
        self.state_data = state_data
        self.event_dict = self.state_data['events']

    def update(self, bot, update):
        _ = set_language(self.language)
        reply = update.message.text

        if reply not in self.event_dict and reply != _('Back'):
            draw_no_such_event_error(self.user, self.language)

        elif reply in self.event_dict:
            return BasicState.create_transition(tags.HACKATHON_MAIN_MENU, event_id=self.event_dict[reply])

        elif reply == _('Back'):
            return BasicState.create_transition(tags.MAIN_MENU)

    def update_inline_req(self, bot, update):
        pass

    def update_inline_ans(self, bot, update):
        pass

    def update_callback(self, bot, update):
        pass

    def exit(self):
        pass
