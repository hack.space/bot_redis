from backend import backend_utils
from backend.language_utils import set_language
from states.basic_state import BasicState
from states.bot_states import tags
from states.state_view_utils.register_email_confirmation_code_view import \
    draw_code_request, \
    draw_verification_code_error, \
    draw_verification_code_success, \
    draw_verification_code_timeout, \
    draw_verification_code_fail, \
    draw_resend_code_fail, \
    draw_resend_code_success, \
    draw_resend_code_try_later


class RegisterEmailConfirmationCodeState(BasicState):
    tag = 'REGISTER_EMAIL_CONFIRMATION_CODE'
    email_regex = r"([a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+)"

    def __init__(self, user):
        # super(RegisterEmailConfirmationCodeState).__init__(user)
        self.user = user
        self.language = self.user.users_db.get_user_field(self.user.user_id, 'language')

    def enter(self, email):
        self.language = self.user.users_db.get_user_field(self.user.user_id, 'language')
        draw_code_request(self.user, self.language)
        self.state_data = {'email': email, 'retry_number': 0}

    def silent_enter(self, state_data):
        self.state_data = state_data

    def update(self, bot, update):
        _ = set_language(self.language)
        reply = update.message.text

        if reply == _("Send the code again"):
            res, try_after = backend_utils.resend_code(self.state_data['email'], self.user.token)

            if res == 404:
                user_data = backend_utils.get_current_user(self.user.token)
                user_data['email'] = self.state_data['email']
                backend_utils.update_current_user(self.user.token, user_data)

                draw_resend_code_fail(self.user, self.state_data['email'], self.language)
            elif res == 429:
                draw_resend_code_try_later(self.user, try_after, self.language)
            else:
                draw_resend_code_success(self.user, self.state_data['email'], self.language)
        elif reply == _("Choose another email address"):
            return BasicState.create_transition(tags.REGISTER_EMAIL, returning=True)
        else:
            res = backend_utils.verify_email(self.state_data['email'], reply, self.user.token)

            if res == 404:
                user_data = backend_utils.get_current_user(self.user.token)
                user_data['email'] = self.state_data['email']
                backend_utils.update_current_user(self.user.token, user_data)

                if self.state_data['retry_number'] >= 5:

                    self.state_data['retry_number'] = 0

                    draw_verification_code_fail(self.user, self.language)
                else:
                    draw_verification_code_timeout(self.user, self.state_data['email'], self.language)
            elif res == 401:
                draw_verification_code_error(self.user, self.language)
                self.state_data['retry_number'] += 1

            else:
                draw_verification_code_success(self.user, self.language)
                return BasicState.create_transition(tags.REGISTER_SKILLS)

    def update_inline_req(self, bot, update):
        pass

    def update_inline_ans(self, bot, update):
        pass

    def update_callback(self, bot, update):
        pass

    def exit(self):
        pass
