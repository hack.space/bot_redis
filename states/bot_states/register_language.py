from states.basic_state import BasicState
from states.bot_states import tags
from states.state_view_utils.register_language import draw_language_request, draw_language_error


class RegisterLanguageState(BasicState):
    tag = 'REGISTER_LANGUAGE'

    def __init__(self, user):
        # super(RegisterLocationState, self).__init__(user)
        self.user = user

    def enter(self, **kwargs):
        draw_language_request(self.user)

    def silent_enter(self, state_data):
        pass

    def update(self, bot, update):
        reply = update.message.text
        if reply not in ['English', 'Russian']:
            draw_language_error(self.user)
            return

        self.user.users_db.set_user_field(self.user.user_id, 'language', reply)
        return BasicState.create_transition(tags.REGISTER_SKILLS)

    def update_inline_req(self, bot, update):
        pass

    def update_inline_ans(self, bot, update):
        pass

    def update_callback(self, bot, update):
        pass

    def exit(self):
        pass
