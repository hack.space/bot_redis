from backend import backend_utils
from backend.language_utils import set_language
from states.basic_state import BasicState
from states.bot_states import tags
from states.state_view_utils.main_menu_view import draw_main_menu, draw_reply_error, draw_no_events, draw_user_profile


class MainMenuState(BasicState):
    tag = 'MAIN_MENU'

    def __init__(self, user):
        # super(MainMenuState, self).__init__(user)
        self.user = user
        self.language = self.user.users_db.get_user_field(self.user.user_id, 'language')
        _ = set_language(self.language)
        self.keyboard_button_names = [_('Show my hackathons'), _('Show my profile')]
        self.keyboard = [[button] for button in self.keyboard_button_names]

    def enter(self, **kwargs):
        self.language = self.user.users_db.get_user_field(self.user.user_id, 'language')
        _ = set_language(self.language)
        self.keyboard_button_names = [_('Show my hackathons'), _('Show my profile')]
        self.keyboard = [[button] for button in self.keyboard_button_names]
        draw_main_menu(self.user, self.keyboard, self.language)

    def silent_enter(self, state_data):
        pass

    def update(self, bot, update):
        _ = set_language(self.language)
        reply = update.message.text

        if reply not in self.keyboard_button_names:
            draw_reply_error(self.user, self.language)

        else:
            if reply == _('Show my hackathons'):
                active_events = backend_utils.get_user_events(self.user.token, 'applied,verified,confirmed,activated')

                if not active_events:
                    draw_no_events(self.user, self.language)
                    return
                else:
                    return BasicState.create_transition(tags.HACKATHON_CHOICE_MENU, events=active_events)
            elif reply == _('Show my profile'):
                user_data = backend_utils.get_current_user(self.user.token)
                draw_user_profile(self.user, user_data, self.language)
                return

    def update_inline_req(self, bot, update):
        pass

    def update_inline_ans(self, bot, update):
        pass

    def update_callback(self, bot, update):
        pass

    def exit(self):
        pass
