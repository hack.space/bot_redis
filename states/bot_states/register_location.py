from backend import backend_utils
from states.basic_state import BasicState
from states.bot_states import tags
from states.state_view_utils.register_location_view import draw_location_request


class RegisterLocationState(BasicState):
    tag = 'REGISTER_LOCATION'

    def __init__(self, user):
        # super(RegisterLocationState, self).__init__(user)
        self.user = user
        self.language = self.user.users_db.get_user_field(self.user.user_id, 'language')

    def enter(self, **kwargs):
        self.language = self.user.users_db.get_user_field(self.user.user_id, 'language')
        draw_location_request(self.user, self.language)

    def silent_enter(self, state_data):
        pass

    def update(self, bot, update):
        reply = update.message.text
        # if reply not in ['Moscow', 'Saint Petersburg']:
        #    draw_location_error(self.user)
        #    return

        user_data = backend_utils.get_current_user(self.user.token)
        user_data['city'] = reply
        backend_utils.update_current_user(self.user.token, user_data)

        return BasicState.create_transition(tags.MAIN_MENU)

    def update_inline_req(self, bot, update):
        pass

    def update_inline_ans(self, bot, update):
        pass

    def update_callback(self, bot, update):
        pass

    def exit(self):
        pass
