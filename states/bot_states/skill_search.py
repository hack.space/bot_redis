from backend import backend_utils
from states.basic_state import BasicState
from states.bot_states import tags
from states.state_view_utils.skill_search_view import draw_search_skills, draw_search_result


class SkillSearchState(BasicState):
    tag = 'SKILL_SEARCH'

    def __init__(self, user):
        # super(SkillSearchState, self).__init__(user)
        self.user = user
        self.event_id = None
        self.language = self.user.users_db.get_user_field(self.user.user_id, 'language')

    def enter(self, event_id):
        self.language = self.user.users_db.get_user_field(self.user.user_id, 'language')
        self.event_id = event_id
        self.state_data = {'event_id': self.event_id}

        skills = backend_utils.get_skills(self.user.token)
        draw_search_skills(self.user, skills, self.language)

    def silent_enter(self, state_data):
        self.state_data = state_data
        self.event_id = self.state_data['event_id']

    def update(self, bot, update):
        reply = update.message.text

        participants = backend_utils.get_participants(self.event_id, self.user.token)

        chosen_participants = [p for p in participants if p['isSearchable'] and reply in p['skills']]

        draw_search_result(self.user, reply, chosen_participants, self.language)

        return BasicState.create_transition(tags.HACKATHON_MAIN_MENU, event_id=self.event_id)

    def update_inline_req(self, bot, update):
        pass

    def update_inline_ans(self, bot, update):
        pass

    def update_callback(self, bot, update):
        pass

    def exit(self):
        pass
