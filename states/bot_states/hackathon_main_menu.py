from backend import backend_utils
from backend.language_utils import set_language
from states.basic_state import BasicState
from states.bot_states import tags
from states.state_view_utils.hackathon_main_menu_view import \
    draw_main_menu_keyboard, \
    draw_event_schedule, \
    draw_user_profile, \
    draw_searchable_switch, \
    draw_finish_already, \
    draw_finish_failed, \
    draw_finish_success, \
    draw_revert_already, \
    draw_revert_failed, \
    draw_revert_success, \
    draw_reply_error, draw_event_vote


class HackathonMainMenuState(BasicState):
    tag = 'HACKATHON_MAIN_MENU'

    def __init__(self, user):
        # super(HackathonMainMenuState, self).__init__(user)
        self.event_id = None
        self.user = user
        self.language = self.user.users_db.get_user_field(self.user.user_id, 'language')

    def enter(self, event_id):
        self.event_id = event_id
        self.state_data = {'event_id': self.event_id}

        event_data = backend_utils.get_event(self.event_id, self.user.token)

        self.language = self.user.users_db.get_user_field(self.user.user_id, 'language')
        draw_main_menu_keyboard(self.user, event_data, self.language)

    def silent_enter(self, state_data):
        self.state_data = state_data
        self.event_id = self.state_data['event_id']

    def update(self, bot, update):
        reply = update.message.text
        _ = set_language(self.language)

        if reply == _('Back'):
            return BasicState.create_transition(tags.MAIN_MENU)
        elif reply == _('Show hackathon schedule'):
            event_data = backend_utils.get_event(self.event_id, self.user.token)
            draw_event_schedule(self.user, event_data)
        elif reply == _('Show my profile'):
            user_data = backend_utils.get_current_user(self.user.token)
            draw_user_profile(self.user, user_data, self.language)
        elif reply in [_('Searchable On'), _('Searchable Off')]:
            new_searchable = backend_utils.toggle_searchable(self.event_id, self.user.token)
            event_data = backend_utils.get_event(self.event_id, self.user.token)

            draw_searchable_switch(self.user, new_searchable, self.language)
            draw_main_menu_keyboard(self.user, event_data, self.language)
        elif reply == _('Search by skill'):
            return BasicState.create_transition(tags.SKILL_SEARCH, event_id=self.event_id)
        elif reply == _('Check me in at the hackathon'):
            return BasicState.create_transition(tags.VERIFY, event_id=self.event_id)
        elif reply == _('Finish my participation'):
            result = backend_utils.participation_status_finish(self.event_id, self.user.token)

            if result == 'failed':
                draw_finish_failed(self.user, self.language)
            elif result == 'already':
                draw_finish_already(self.user, self.language)
            elif result == 'participated':
                draw_finish_success(self.user, self.language)

            event_data = backend_utils.get_event(self.event_id, self.user.token)
            draw_main_menu_keyboard(self.user, event_data, self.language)
        elif reply == _('Check me back in at the hackathon'):
            result = backend_utils.participation_status_revert(self.event_id, self.user.token)

            if result == 'failed':
                draw_revert_failed(self.user, self.language)
            elif result == 'already':
                draw_revert_already(self.user, self.language)
            elif result == 'activated':
                draw_revert_success(self.user, self.language)

            event_data = backend_utils.get_event(self.event_id, self.user.token)
            draw_main_menu_keyboard(self.user, event_data, self.language)
        elif reply == _('Vote \U0001F64B'):
            event_data = backend_utils.get_event(self.event_id, self.user.token)
            draw_event_vote(self.user, event_data, self.language)
        else:
            draw_reply_error(self.user, self.language)
            
    def update_inline_req(self, bot, update):
        pass

    def update_inline_ans(self, bot, update):
        pass

    def update_callback(self, bot, update):
        pass

    def exit(self):
        pass
