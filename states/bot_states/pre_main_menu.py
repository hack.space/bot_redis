from states.basic_state import BasicState
from states.bot_states import tags
from states.state_view_utils.pre_main_menu_view import welcome_back_reply


class PreMainMenuState(BasicState):
    tag = 'PRE_MAIN_MENU'

    def __init__(self, user):
        # super(PreMainMenuState, self).__init__(user)
        self.user = user
        self.language = self.user.users_db.get_user_field(self.user.user_id, 'language')

    def enter(self, **kwargs):
        self.language = self.user.users_db.get_user_field(self.user.user_id, 'language')

    def update(self, bot, update):
        welcome_back_reply(self.user, update.message.text, self.language)
        return BasicState.create_transition(tags.MAIN_MENU)

    def update_inline_req(self, bot, update):
        pass

    def update_inline_ans(self, bot, update):
        pass

    def update_callback(self, bot, update):
        pass

    def exit(self):
        pass
