from backend import backend_utils
from states.basic_state import BasicState
from states.bot_states import tags
from states.state_view_utils.verify_view import \
    draw_verify_choice, \
    draw_location_activate_failed, \
    draw_activate_already, \
    draw_location_activate_success, \
    draw_reply_error


class VerifyState(BasicState):
    tag = 'VERIFY'

    def __init__(self, user):
        # super(VerifyState, self).__init__(user)
        self.user = user
        self.event_id = None
        self.language = self.user.users_db.get_user_field(self.user.user_id, 'language')

    def enter(self, event_id):
        self.language = self.user.users_db.get_user_field(self.user.user_id, 'language')
        draw_verify_choice(self.user, self.language)

        self.event_id = event_id
        self.state_data = {'event_id': self.event_id}

    def silent_enter(self, state_data):
        self.state_data = state_data
        self.event_id = self.state_data['event_id']

    def update(self, bot, update):
        reply = update.message.text
        location = update.message.location

        if reply == 'Back':
            return BasicState.create_transition(tags.HACKATHON_MAIN_MENU, event_id=self.event_id)

        if reply == 'Password':
            return BasicState.create_transition(tags.VERIFY_PASSWORD, event_id=self.event_id)

        elif not reply and location:

            result = backend_utils.participation_status_activate(self.user.token, self.event_id, reply, location)

            if result == 'failed':
                draw_location_activate_failed(self.user)
            elif result == 'already':
                draw_activate_already(self.user, self.language)
            elif result == 'activated':
                draw_location_activate_success(self.user, self.language)

            return BasicState.create_transition(tags.HACKATHON_MAIN_MENU, event_id=self.event_id)

        else:
            draw_reply_error(self.user, self.language)

    def update_inline_req(self, bot, update):
        pass

    def update_inline_ans(self, bot, update):
        pass

    def update_callback(self, bot, update):
        pass

    def exit(self):
        pass
