from backend import backend_utils
from states.basic_state import BasicState
from states.bot_states import tags
from states.state_view_utils.verify_password_view import \
    draw_password_request, \
    draw_password_activate_failed, \
    draw_activate_already, \
    draw_password_activate_success


class VerifyPasswordState(BasicState):
    tag = 'VERIFY_PASSWORD'

    def __init__(self, user):
        # super(VerifyPasswordState, self).__init__(user)
        self.user = user
        self.event_id = None
        self.language = self.user.users_db.get_user_field(self.user.user_id, 'language')

    def enter(self, event_id):
        self.language = self.user.users_db.get_user_field(self.user.user_id, 'language')
        draw_password_request(self.user, self.language)

        self.event_id = event_id
        self.state_data = {'event_id': self.event_id}

    def silent_enter(self, state_data):
        self.state_data = state_data
        self.event_id = self.state_data['event_id']

    def update(self, bot, update):
        reply = update.message.text

        result = backend_utils.participation_status_activate(self.user.token, self.event_id, reply, None)

        if result == 'failed':
            draw_password_activate_failed(self.user, self.language)
        elif result == 'already':
            draw_activate_already(self.user, self.language)
        elif result == 'activated':
            draw_password_activate_success(self.user, self.language)

        return BasicState.create_transition(tags.HACKATHON_MAIN_MENU, event_id=self.event_id)

    def update_inline_req(self, bot, update):
        pass

    def update_inline_ans(self, bot, update):
        pass

    def update_callback(self, bot, update):
        pass

    def exit(self):
        pass
