from backend import backend_utils
from states.basic_state import BasicState
from states.bot_states import tags
from states.state_view_utils.register_start_view import draw_greetings


class RegisterStartState(BasicState):
    tag = 'REGISTER_START'

    def __init__(self, user):
        # super(RegisterStartState, self).__init__(user)
        self.user = user

    def enter(self, **kwargs):
        self.language = self.user.users_db.get_user_field(self.user.user_id, 'language')

    def update(self, bot, update):
        draw_greetings(self.user)
        user_data = backend_utils.get_current_user(self.user.token)
        backend_utils.update_current_user(self.user.token, user_data)
        return BasicState.create_transition(tags.REGISTER_LANGUAGE)

    def update_inline_req(self, bot, update):
        pass

    def update_inline_ans(self, bot, update):
        pass

    def update_callback(self, bot, update):
        pass

    def exit(self):
        pass
