import re

from backend import backend_utils
from backend.language_utils import set_language
from states.basic_state import BasicState
from states.bot_states import tags
from states.state_view_utils.register_skills_view import draw_skills_request, draw_skill_selected, draw_skill_error, \
    draw_too_many_skills


class RegisterSkillsState(BasicState):
    tag = 'REGISTER_SKILLS'

    def __init__(self, user):
        # super(RegisterSkillsState, self).__init__(user)
        self.user = user
        skills = backend_utils.get_skills(self.user.token)
        self.skill_dict = {skill['tag']: {'id': skill['id'], 'chosen': False} for skill in skills}
        self.state_data = {'skills': self.skill_dict}
        self.language = self.user.users_db.get_user_field(self.user.user_id, 'language')

    def enter(self, **kwargs):
        self.language = self.user.users_db.get_user_field(self.user.user_id, 'language')
        skills = backend_utils.get_skills(self.user.token)
        self.skill_dict = {skill['tag']: {'id': skill['id'], 'chosen': False} for skill in skills}
        self.state_data = {'skills': self.skill_dict}
        draw_skills_request(self.user, self.skill_dict, self.language)

    def silent_enter(self, state_data):
        self.language = self.user.users_db.get_user_field(self.user.user_id, 'language')
        self.state_data = state_data
        self.skill_dict = state_data['skills']

    def update(self, bot, update):
        _ = set_language(self.language)
        reply = update.message.text
        if reply != _('Done!'):
            chosen_skill = re.sub('^\U00002714 ', '', reply)
            if chosen_skill not in self.skill_dict:
                draw_skill_error(self.user, chosen_skill, self.language)
                return

            self.skill_dict[chosen_skill]['chosen'] = not self.skill_dict[chosen_skill]['chosen']
            self.state_data = {'skills': self.skill_dict}

            draw_skill_selected(self.user, self.skill_dict, chosen_skill, self.skill_dict[chosen_skill]['chosen'],
                                self.language)
            return
        else:
            user_data = backend_utils.get_current_user(self.user.token)

            skill_set = [{'id': self.skill_dict[skill]['id']} for skill in self.skill_dict if
                         self.skill_dict[skill]['chosen']]
            if len(skill_set) > 3:
                draw_too_many_skills(self.user, self.language)
                return

            user_data['skills'] = [{'id': self.skill_dict[skill]['id']} for skill in self.skill_dict if
                                   self.skill_dict[skill]['chosen']]
            backend_utils.update_current_user(self.user.token, user_data)

            return BasicState.create_transition(tags.MAIN_MENU)

    def update_inline_req(self, bot, update):
        pass

    def update_inline_ans(self, bot, update):
        pass

    def update_callback(self, bot, update):
        pass

    def exit(self):
        pass
