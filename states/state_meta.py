class WrongStateException(Exception):
    pass


class StateMeta(type):
    states = {}

    def __new__(mcs, class_name, base_classes, param_dict):
        print("Creating class {}".format(class_name))

        element = type.__new__(mcs, class_name, base_classes, param_dict)
        if 'tag' not in param_dict or not isinstance(param_dict['tag'], str):
            raise WrongStateException

        tag = param_dict['tag']

        if tag != '':
            StateMeta.states[tag] = element

        return element
