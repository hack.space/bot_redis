import re

import emoji
from telegram import ReplyKeyboardMarkup, InlineKeyboardButton, InlineKeyboardMarkup

from backend.language_utils import set_language

EMOJI_MAIL = emoji.emojize(':e-mail:')
EMOJI_PHONE = '\U0000260E'
EMOJI_BIO = '\U0001F393'
EMOJI_STAT = emoji.emojize(':bar_chart:')
EMOJI_HACK_TOTAL = emoji.emojize(':heavy_check_mark:')
EMOJI_HACK_WIN = emoji.emojize(':trophy:')
EMOJI_XP = '\U0001F3AF'
EMOJI_COINS = '\U0001F4B0'
EMOJI_SKILLS = emoji.emojize(':glowing_star:')
EMOJI_SKILL_VERIFIED = '\U0001F44D'


def draw_main_menu(user, keyboard, language):
    _ = set_language(language)
    user.send_message(
        _('What do you want me to do?'),
        reply_markup=ReplyKeyboardMarkup(keyboard)
    )


def draw_user_profile(user, user_data, language):
    _ = set_language(language)
    email = _('Email')
    phone = _('Phone')
    bio = _('Bio')
    stats = _('Stats')
    participations = _('Participations')
    victories = _('Victories')
    xp = _('XP')
    coins = _('Coins')
    skills = _('Skills')
    verified = _('Verified by')
    profile_string = ""
    profile_string += '<b>{}</b>\n\n'.format(re.sub('\s*(null|undefined)\s*', '', user_data['username']))
    profile_string += '{} <b>{}:</b> {}\n\n'.format(EMOJI_MAIL, email, user_data['email']) if user_data['email'] else ''
    profile_string += '{} <b>{}:</b> {}\n\n'.format(EMOJI_PHONE, phone, user_data['contactPhone']) if user_data[
        'contactPhone'] else ''
    profile_string += '{} <b>{}:</b>\n{}\n\n'.format(EMOJI_BIO, bio, user_data['bio']) if user_data['bio'] else ''
    profile_string += '{} <b>{}:</b>\n'.format(EMOJI_STAT, stats)
    profile_string += '    {} <b>{}: </b>{}\n'.format(EMOJI_HACK_TOTAL, participations, user_data['stat']['hackTotal'])
    profile_string += '    {} <b>{}: </b>{}\n'.format(EMOJI_HACK_WIN, victories, user_data['stat']['hackWin'])
    profile_string += '    {} <b>{}: </b>{}\n'.format(EMOJI_XP, xp, user_data['stat']['xp'])
    profile_string += '    {} <b>{}: </b>{}\n\n'.format(EMOJI_COINS, coins, user_data['stat']['coins'])
    profile_string += '{} <b>{}:</b>\n'.format(EMOJI_SKILLS, skills)
    for skill in user_data['skills']:
        profile_string += '    <b>{} - {} {}: {} </b>\n'.format(
            skill['tag'],
            EMOJI_SKILL_VERIFIED,
            verified,
            skill['verified']
        )

    external_profile_url = 'http://hackathons.space/profile/t/' + user_data['tgProfileLink'].split('/')[-1]
    inline_keyboard = [[InlineKeyboardButton(_('Go to profile page'), url=external_profile_url)]]
    user.send_message(profile_string, parse_mode='HTML', reply_markup=InlineKeyboardMarkup(inline_keyboard))


def draw_no_events(user, language):
    _ = set_language(language)
    hackathons_url = 'http://hackathons.space/hackathons'
    inline_keyboard = [[InlineKeyboardButton(_('See open hackathons'), url=hackathons_url)]]

    user.send_message(
        _(
            'Sorry, you don\'t have any active hackathons \U0001F61F. But you can see all available hackathons on our website!'),
        reply_markup=InlineKeyboardMarkup(inline_keyboard)
    )


def draw_reply_error(user, language):
    _ = set_language(language)
    user.send_message(
        _('Sorry, I don\'t know what you mean \U0001F61F')
    )
