from telegram import InlineKeyboardMarkup, InlineKeyboardButton

from backend.language_utils import set_language
from config.bot_config import USER_AGREEMENT


def draw_email_request(user, language):
    _ = set_language(language)
    user.send_message(
        _('Oh gosh, it seems we don\'t know you! Not to worry, I\'ve got you covered. First,'
          ' tell me your email! I\'ll send you a confirmation code. By sending this email to us, '
          'you agree to our terms and conditions.'),
        reply_markup=InlineKeyboardMarkup([[InlineKeyboardButton(text='User agreement', url=USER_AGREEMENT)]])
    )


def draw_returning_email_request(user, language):
    _ = set_language(language)
    user.send_message(
        _(
            'Please tell me another email. Don\'t forget that I\'ll send you a confirmation code! By sending this email to '
            'us, you agree to our terms and conditions.'),
        reply_markup=InlineKeyboardMarkup([[InlineKeyboardButton(text='User agreement', url=USER_AGREEMENT)]])
    )


def draw_email_error(user, language):
    _ = set_language(language)
    user.send_message(
        _("Looks like you didn't send a valid e-mail. Could you please try again?")
    )


def draw_email_set(user, email, language):
    _ = set_language(language)
    user.send_message(
        _('I\'ve set your email to ') + email + '!'
    )
