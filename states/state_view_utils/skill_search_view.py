import gettext
import re

from telegram import ReplyKeyboardMarkup

from backend.language_utils import set_language

gettext.install('messages', './locale')


def draw_search_skills(user, skills, language):
    _ = set_language(language)
    keyboard = [[skill['tag']] for skill in skills]
    user.send_message(
        _('What skill do you want to search for?'),
        reply_markup=ReplyKeyboardMarkup(keyboard)
    )


def draw_search_result(user, skill, participants, language):
    _ = set_language(language)
    result_string = '<b>{}</b>\n\n'.format(skill)
    if not participants:
        result_string += _('Sorry, we haven\'t found anyone :(')

    for participant in participants:
        if participant['tgProfileLink'] == 'null':
            continue
        tg_handle = _('TG Handle')
        result_string += '<b>{}</b>\n'.format(re.sub(' *null *', '', participant['username']))
        result_string += '<b>{}:</b> @{}\n'.format(tg_handle, participant['tgProfileLink'].split('/')[-1])
        result_string += '<b>XP:</b> {}\n\n'.format(participant['xp'])

    user.send_message(result_string, parse_mode='HTML')
