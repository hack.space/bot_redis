import re

import emoji
from telegram import ReplyKeyboardMarkup, InlineKeyboardMarkup, InlineKeyboardButton

from backend.language_utils import set_language

EMOJI_MAIL = emoji.emojize(':e-mail:')
EMOJI_PHONE = '\U0000260E'
EMOJI_BIO = '\U0001F393'
EMOJI_STAT = emoji.emojize(':bar_chart:')
EMOJI_HACK_TOTAL = emoji.emojize(':heavy_check_mark:')
EMOJI_HACK_WIN = emoji.emojize(':trophy:')
EMOJI_XP = '\U0001F3AF'
EMOJI_COINS = '\U0001F4B0'
EMOJI_SKILLS = emoji.emojize(':glowing_star:')
EMOJI_SKILL_VERIFIED = '\U0001F44D'


def draw_main_menu_keyboard(user, event_data, language):
    _ = set_language(language)
    searchable_string = _('Searchable On') if event_data['isSearchable'] else _('Searchable Off')

    keyboard = [[_('Search by skill'), searchable_string]]

    change_status_string = None
    if event_data['status'] in ['applied', 'verified', 'confirmed']:
        change_status_string = _('Check me in at the hackathon')
    elif event_data['status'] == 'activated':
        change_status_string = _('Finish my participation')
    elif event_data['status'] in ['participated', 'won']:
        change_status_string = _('Check me back in at the hackathon')

    if 'judgingLink' in event_data and event_data.get('judgingLink'):
        third_button = [change_status_string, _('Vote \U0001F64B')]
    else:
        third_button = [change_status_string]

    keyboard += [[_('Show hackathon schedule')], third_button, [_('Show my profile')], [_('Back')]]

    user.send_message(
        _('What do you want me to do?'),
        reply_markup=ReplyKeyboardMarkup(keyboard)
    )


def draw_searchable_switch(user, new_searchable, language):
    _ = set_language(language)
    # can_search_string = 'can' if new_searchable else 'can\'t'
    if new_searchable:
        user.send_message(_('Now others can search you by skill!'))
    else:
        user.send_message(_('Now others can\'t search you by skill!'))


def draw_event_schedule(user, event_data):
    user.send_message(
        event_data['schedule']
    )


def draw_user_profile(user, user_data, language):
    _ = set_language(language)
    email = _('Email')
    phone = _('Phone')
    bio = _('Bio')
    stats = _('Stats')
    participations = _('Participations')
    victories = _('Victories')
    xp = _('XP')
    coins = _('Coins')
    skills = _('Skills')
    verified = _('Verified by')
    profile_string = ""
    profile_string += '<b>{}</b>\n\n'.format(re.sub('\s*(null|undefined)\s*', '', user_data['username']))
    profile_string += '{} <b>{}:</b> {}\n\n'.format(EMOJI_MAIL, email, user_data['email']) if user_data['email'] else ''
    profile_string += '{} <b>{}:</b> {}\n\n'.format(EMOJI_PHONE, phone, user_data['contactPhone']) if user_data[
        'contactPhone'] else ''
    profile_string += '{} <b>{}:</b>\n{}\n\n'.format(EMOJI_BIO, bio, user_data['bio']) if user_data['bio'] else ''
    profile_string += '{} <b>{}:</b>\n'.format(EMOJI_STAT, stats)
    profile_string += '    {} <b>{}: </b>{}\n'.format(EMOJI_HACK_TOTAL, participations, user_data['stat']['hackTotal'])
    profile_string += '    {} <b>{}: </b>{}\n'.format(EMOJI_HACK_WIN, victories, user_data['stat']['hackWin'])
    profile_string += '    {} <b>{}: </b>{}\n'.format(EMOJI_XP, xp, user_data['stat']['xp'])
    profile_string += '    {} <b>{}: </b>{}\n\n'.format(EMOJI_COINS, coins, user_data['stat']['coins'])
    profile_string += '{} <b>{}:</b>\n'.format(EMOJI_SKILLS, skills)
    for skill in user_data['skills']:
        profile_string += '    <b>{} - {} {}: {} </b>\n'.format(
            skill['tag'],
            EMOJI_SKILL_VERIFIED,
            verified,
            skill['verified']
        )

    external_profile_url = 'http://hackathons.space/profile/t/' + user_data['tgProfileLink'].split('/')[-1]
    inline_keyboard = [[InlineKeyboardButton(_('Go to profile page'), url=external_profile_url)]]
    user.send_message(profile_string, parse_mode='HTML', reply_markup=InlineKeyboardMarkup(inline_keyboard))


def draw_finish_success(user, language):
    _ = set_language(language)
    user.send_message(
        _('You\'ve successfully ended your participation! You will no longer get messages from organizers. If you'
          ' want to, check back into the hackathon before it is over!')
    )


def draw_finish_already(user, language):
    _ = set_language(language)
    user.send_message(
        _('It seems you have already finished your participation \U0001F615. Do you want to check back in instead?')
    )


def draw_finish_failed(user, language):
    _ = set_language(language)
    user.send_message(
        _('It seems you are still not activated and should not have even seen this button \U0001F615 Could you'
          ' please write to @peramor about this?')
    )


def draw_revert_success(user, language):
    _ = set_language(language)
    user.send_message(
        _('You have checked back into the hackathon \u263A You can again receive messages from the organizers.')
    )


def draw_revert_already(user, language):
    _ = set_language(language)
    user.send_message(
        _('It seems you are already activated \U0001F615. Do you want to finish instead?')
    )


def draw_revert_failed(user, language):
    _ = set_language(language)
    user.send_message(
        _('Sorry, you cannot check back in, as the hackathon has ended \U0001F641')
    )


def draw_reply_error(user, language):
    _ = set_language(language)
    user.send_message(
        _('Sorry, I don\'t know what you mean \U0001F61F')
    )


def draw_event_vote(user, event_data, language):
    _ = set_language(language)
    if 'judgingEnabled' in event_data and event_data.get('judgingEnabled'):
        message = _('Here you can vote for teams.')
        inline_keyboard = [[InlineKeyboardButton(_('Vote'), url=event_data.get('judgingLink'))]]
        user.send_message(message, parse_mode='HTML', reply_markup=InlineKeyboardMarkup(inline_keyboard))
    else:
        message = _('Voting hasn\'t begun yet. Please, try later')
        user.send_message(message, parse_mode='HTML')
