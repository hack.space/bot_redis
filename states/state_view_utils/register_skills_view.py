from telegram import ReplyKeyboardMarkup

from backend.language_utils import set_language


def draw_skills_request(user, skills, language):
    _ = set_language(language)
    reply_keyboard = [[skill] for skill in skills]
    user.send_message(
        _('Now, please tell me what you can do! You have to choose at least one of the skills below and'
          ' press \"Done!\". But I know you\'re awesome and will pick more \U0001F609.'),
        reply_markup=ReplyKeyboardMarkup(reply_keyboard)
    )


def draw_skill_selected(user, skills, choice, switched_on, language):
    _ = set_language(language)
    reply_keyboard = [['\U00002714 ' + skill] if skills[skill]['chosen'] else [skill] for skill in skills]
    if sum([1 if skills[skill]['chosen'] else 0 for skill in skills]):
        reply_keyboard.append([_('Done!')])
    youve = _('You\'ve ' + ('chosen' if switched_on else 'discarded'))
    user.send_message(
        '{} \"{}\"'.format(youve, choice),
        reply_markup=ReplyKeyboardMarkup(reply_keyboard)
    )


def draw_skill_error(user, choice, language):
    _ = set_language(language)
    user.send_message(
        _('I\'m sorry, I don\'t seem to know what ') + f'\"{choice}\"' + _(
            ' is \U0001F61F. If you want to add this skill'
            f' to the list below, please write to my creator: @peramor.')
    )


def draw_too_many_skills(user, language):
    _ = set_language(language)
    user.send_message(
        _('Don\'t you think that more than 3 skills is a bit too much? Try to concentrate on your'
          ' biggest strengths - that\'s a sure way to succeed in life!')
    )
