from telegram import ReplyKeyboardMarkup


def draw_language_request(user):
    reply_keyboard = [['English'], ['Russian']]
    user.send_message(
        'Please, select language',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard)
    )


def draw_language_error(user):
    user.send_message('Sorry, but I don\t know this language \U0001F61F. For now please choose from the list below.')
