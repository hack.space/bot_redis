import gettext

from telegram import ReplyKeyboardMarkup, KeyboardButton

from backend.language_utils import set_language

gettext.install('messages', './locale')


def draw_verify_choice(user, language):
    _ = set_language(language)
    keyboard = [['Password'], [KeyboardButton('Location', request_location=True)], ['Back']]
    user.send_message(
        _('Do you want to check in through entering the password or location sharing?'),
        reply_markup=ReplyKeyboardMarkup(keyboard)
    )


def draw_location_activate_failed(user, language):
    _ = set_language(language)
    user.send_message(
        _('Sorry, the location was incorrect \U0001F641')
    )


def draw_activate_already(user, language):
    _ = set_language(language)
    user.send_message(
        _('It seems you are already activated \U0001F615. Do you want to finish instead?')
    )


def draw_location_activate_success(user, language):
    _ = set_language(language)
    user.send_message(
        _('The location was correct \u263A Welcome to the hackathon!')
    )


def draw_reply_error(user, language):
    _ = set_language(language)
    user.send_message(
        _('Sorry, I don\'t know what you mean \U0001F61F')
    )
