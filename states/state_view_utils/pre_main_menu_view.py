from backend.language_utils import set_language


def welcome_back_reply(user, text, language):
    _ = set_language(language)
    user.send_message(
        _('And') + f'\"{text}\"' + _('to you as well, tiger \U0001F609. Welcome back!')
    )
