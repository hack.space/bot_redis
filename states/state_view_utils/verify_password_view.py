import gettext

from telegram import ReplyKeyboardRemove

from backend.language_utils import set_language

gettext.install('messages', './locale')


def draw_password_request(user, language):
    _ = set_language(language)
    user.send_message(
        _('Please enter the password:'),
        reply_markup=ReplyKeyboardRemove()
    )


def draw_password_activate_failed(user, language):
    _ = set_language(language)
    user.send_message(
        _('Sorry, the password was incorrect \U0001F641')
    )


def draw_activate_already(user, language):
    _ = set_language(language)
    user.send_message(
        _('It seems you are already activated \U0001F615. Do you want to finish instead?')
    )


def draw_password_activate_success(user, language):
    _ = set_language(language)
    user.send_message(
        _('The password was correct \u263A Welcome to the hackathon!')
    )
