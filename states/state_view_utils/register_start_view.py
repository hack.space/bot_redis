import gettext

from telegram import ReplyKeyboardRemove

gettext.install('messages', './locale')

def draw_greetings(user):
    user.send_message(_('Hi! Nice to meet you!'), reply_markup=ReplyKeyboardRemove())
