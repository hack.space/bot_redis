from telegram import ReplyKeyboardMarkup

from backend.language_utils import set_language


def draw_event_choice_keyboard(user, events, language):
    _ = set_language(language)

    keyboard = [[event] for event in events]
    keyboard += [[_('Back')]]

    user.send_message(
        _('What hackathon do you want to see?'),
        reply_markup=ReplyKeyboardMarkup(keyboard)
    )


def draw_no_such_event_error(user, language):
    _ = set_language(language)
    user.send_message(
        _('Sorry, it seems you aren\'t applied to such hackathon \U0001F61F. If you are sure you\'ve applied to this'
          ' hackathon and it\'s not on the list, please contact my creator @peramor.')
    )
