from telegram import ReplyKeyboardRemove

from backend.language_utils import set_language


def draw_location_request(user, language):
    _ = set_language(language)
    # reply_keyboard = [['Moscow'], ['Saint Petersburg']]
    user.send_message(
        _('Now, maybe you could tell me where you live?'),
        reply_markup=ReplyKeyboardRemove()
    )


def draw_location_error(user, language):
    _ = set_language(language)
    user.send_message(
        _('Sorry, but I don\t know this place \U0001F61F. For now please choose from the list below.')
    )
