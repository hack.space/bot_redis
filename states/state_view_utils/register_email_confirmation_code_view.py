from telegram import ReplyKeyboardMarkup, ReplyKeyboardRemove

from backend.language_utils import set_language


def draw_code_request(user, language):
    _ = set_language(language)
    keyboard = [[_('Send the code again')], [_('Choose another email address')]]
    user.send_message(
        _(
            'Please enter the code from the email I\'ve sent you (check the spam folder just in case). If you don\'t receive the email within 1 minute, just ask me to send the code again.'),
        reply_markup=ReplyKeyboardMarkup(keyboard)
    )


def draw_verification_code_timeout(user, email, language):
    _ = set_language(language)
    user.send_message(
        _(
            'Oops, looks like the code you\'ve sent is too old. Don\'t worry, I\'ve sent you another one to ') + email + '!'
    )


def draw_verification_code_error(user, language):
    _ = set_language(language)
    user.send_message(
        _('Looks like this code is incorrect \U0001F61F Could you please try again?')
    )


def draw_verification_code_success(user, language):
    _ = set_language(language)
    user.send_message(
        _('Email successfully verified!'),
        reply_markup=ReplyKeyboardRemove()
    )


def draw_verification_code_fail(user, language):
    _ = set_language(language)
    user.send_message(
        _('It seems that you\'re not having a lot of luck with this code. I\'ve sent you another!')
    )


def draw_resend_code_fail(user, email, language):
    _ = set_language(language)
    user.send_message(
        _('Oops, it looks like your code already expired. Don\'t worry, I\'ve sent you another one to ') + email + '!'
    )


def draw_resend_code_try_later(user, try_after, language):
    _ = set_language(language)
    user.send_message(
        _('Easy, friend, you\'re asking for too many resends \U0001F609 Please wait ') + try_after + _(
            ' s. before asking again.')
    )


def draw_resend_code_success(user, email, language):
    _ = set_language(language)
    user.send_message(
        _('I\'ve sent you another code to ') + email + '!'
    )
