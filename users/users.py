import json
import time

from telegram.error import BadRequest

from backend import backend_utils
from database import database_utils
from states.bot_states import tags
from states.state_machine import StateMachine


class User:
    def __init__(self, user_id, users_db, user_data, bot):

        self.users_db = users_db
        self.bot = bot

        self.user_id = user_id
        self.token = user_data['token']
        self.token_received = user_data['token_received']

        self.state_machine = StateMachine(self, user_data['state'], user_data['state_data'])
        self.main_menu_message = None

    def send_message(self, text, *args, **kwargs):
        return self.bot.sendMessage(*args, text=text, chat_id=self.user_id, **kwargs)

    def remove_keyboard(self, message_id):
        try:
            return self.bot.editMessageReplyMarkup(chat_id=self.user_id,
                                                   message_id=message_id)
        except BadRequest:
            return

    def edit_message(self, message_id, text, *args, **kwargs):
        return self.bot.editMessageText(*args, chat_id=self.user_id,
                                        message_id=message_id,
                                        text=text,
                                        **kwargs)

    def send_or_edit_message(self, *args, invoke=True, **kwargs):
        if self.main_menu_message is None:
            invoke = True

        if self.main_menu_message is not None and invoke:
            self.remove_keyboard(self.main_menu_message)
            self.main_menu_message = None

        if invoke:
            result = self.send_message(*args, **kwargs)
            self.main_menu_message = result.message_id
            return result
        else:
            return self.edit_message(self.main_menu_message, *args, **kwargs)

    def process_message(self, bot, update):
        self.state_machine.process_message(bot, update)

    def process_callback(self, bot, update):
        self.state_machine.process_callback(bot, update)


class UserManager:
    def __init__(self, bot):
        self.users_dict = dict()
        self.users_db = database_utils.UserDatabase()
        self.bot = bot

    def login(self, user):

        auth_timestamp = int(time.time())

        tg_user_data = {
            "id": user['id'],
            "username": user['username'],
            "auth_date": auth_timestamp,
        }

        if user['first_name']:
            tg_user_data["first_name"] = user['first_name']

        if user['last_name']:
            tg_user_data["last_name"] = user['last_name']

        try:
            file_id = user.get_profile_photos()['photos'][0][-1]['file_id']
            tg_user_data["photo_url"] = self.bot.getFile(file_id)['file_path']
        except:
            pass

        token, user_data = backend_utils.user_login(tg_user_data)

        return token, auth_timestamp, user_data

    def get_user(self, user):

        user_id = user['id']

        if user_id not in self.users_dict:
            user_state_data = self.users_db.get_user(user_id)

            if not user_state_data:

                token, auth_timestamp, user_data = self.login(user)

                initial_tag = tags.FALLBACK_STATE if 'email' not in user_data or not user_data[
                    'email'] else tags.DEFAULT_STATE

                user_state_data = {'state': initial_tag,
                                   'state_data': {},
                                   'token': token,
                                   'token_received': auth_timestamp}

                self.users_dict[user_id] = User(user_id, self.users_db, user_state_data, self.bot)
                self.users_db.add_user(user_id, user_state_data)

            else:
                user_state_data['state_data'] = json.loads(user_state_data['state_data'])
                user_state_data['token_received'] = int(user_state_data['token_received'])

                self.users_dict[user_id] = User(user_id, self.users_db, user_state_data, self.bot)

        if int(time.time()) - self.users_dict[user_id].token_received > 60 * 60 * 24 * 50:
            token, auth_timestamp, _ = self.login(user)
            self.users_dict[user_id].token = token
            self.users_dict[user_id].token_received = auth_timestamp

        return self.users_dict[user_id]
