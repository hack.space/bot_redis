from telegram import TelegramError
from telegram.ext import Updater
from telegram.ext import MessageHandler, CallbackQueryHandler, CommandHandler
from telegram.ext.filters import Filters
from users.users import UserManager
from config import bot_config
from push_message_server import push_message_handlers
from states.bot_states import tags
from misc_handlers.event_apply_handler import event_apply_handler
import logging
import json
import sys
import re


logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
logger = logging.getLogger(__name__)
logger.addHandler(logging.StreamHandler(sys.stdout))


class Main:
    def __init__(self):
        self.telegram_updater = Updater(token=bot_config.TOKEN)

        self.user_manager = UserManager(self.telegram_updater.bot)

        self.telegram_updater.dispatcher.add_handler(
            CommandHandler('start', self.event_apply_handler, pass_args=True),
            group=0
        )
        self.telegram_updater.dispatcher.add_handler(MessageHandler(Filters.all, self.message_handler), group=0)
        self.telegram_updater.dispatcher.add_handler(CallbackQueryHandler(self.callback_handler), group=0)
        self.telegram_updater.dispatcher.add_error_handler(self.telegram_error)

        self.telegram_updater.start_polling()
        self.telegram_updater.idle()

    def event_apply_handler(self, bot, update, args):
        if not args:
            user = self.user_manager.get_user(update.message.from_user)
            if user.state_machine.state.tag == tags.FALLBACK_STATE:
                self.message_handler(bot, update)
                return

            user.state_machine.state.enter()
            return

        event_id = args[0]
        user = self.user_manager.get_user(update.message.from_user)
        event_apply_handler(event_id, user)

        if user.state_machine.state.tag == tags.FALLBACK_STATE:
            self.message_handler(bot, update)

    def message_handler(self, bot, update):
        self.user_manager.get_user(update.message.from_user).process_message(bot, update)

    def callback_handler(self, bot, update):
        callback_data = update.callback_query.data

        if re.match('ps_m', callback_data):
            push_message_handlers.process(bot, update, callback_data)
        else:
            self.user_manager.get_user(update.callback_query.from_user).process_callback(bot, update)

    def telegram_error(self, bot, update, error):
        try:
            raise error
        except TelegramError as details:
            raise


if __name__ == "__main__":
    Main()
