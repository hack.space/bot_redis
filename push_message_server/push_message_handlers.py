from telegram import ReplyKeyboardRemove


class UnknownResponseError:
    pass


def process(bot, update, callback_data):
    if callback_data.split('/')[1] == 'ver':
        process_verify(bot, update, callback_data)

    else:
        raise UnknownResponseError


def process_verify(bot, update, callback_data):
    msg = update.callback_query.message

    answer = callback_data.split('/')[2]

    if answer == 'yes':
        msg.edit_text('Great! See you at the hackathon \u263A')
    elif answer == 'maybe':
        msg.edit_text('I hope you\'ll make it!')
    elif answer == 'no':
        msg.edit_text('I\'m sorry to hear that \U0001F641')
