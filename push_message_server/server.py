from config import bot_config

from flask import Flask, request, jsonify
from telegram.bot import Bot
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
import json

app = Flask(__name__)
bot = Bot(bot_config.TOKEN)


@app.route(f'/{bot_config.TOKEN}/send_messages', methods=['POST'])
def send_messages():
    messages = json.loads(request.data)
    reply = {}

    for message in messages:
        try:
            bot.send_message(message['user_tg_id'], message['text'])
            reply[message['user_tg_id']] = 'success'
        except:
            reply[message['user_tg_id']] = 'failed'

    return jsonify(reply)


@app.route(f'/{bot_config.TOKEN}/send_verify_requests', methods=['POST'])
def send_verify_requests():
    messages = json.loads(request.data)
    reply = {}

    for message in messages:
        try:
            button_yes = InlineKeyboardButton('Yes',
                                              callback_data='ps_m/ver/yes'
                                              )
            button_maybe = InlineKeyboardButton('Maybe',
                                                callback_data='ps_m/ver/maybe'
                                                )

            button_no = InlineKeyboardButton('No',
                                             callback_data='ps_m/ver/no'
                                             )
            inline_keyboard = [[button_yes, button_maybe, button_no]]

            bot.send_message(message['user_tg_id'], message['text'], reply_markup=InlineKeyboardMarkup(inline_keyboard))
            reply[message['user_tg_id']] = 'success'
        except Exception as e:
            print(e)
            reply[message['user_tg_id']] = 'failed'

    return jsonify(reply)


app.run('localhost', 4999)
