FROM python:3.6

# Create app folder
RUN mkdir -p /usr/src/app

# Set PWD to the app folder
WORKDIR /usr/src/app

# Bundle app source
COPY . /usr/src/app

# Install dependencies
RUN pip install -r requirements.txt

CMD [ "python", "main.py" ]