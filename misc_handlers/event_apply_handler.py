from backend.backend_utils import apply_for_event, get_event


def event_apply_handler(event_id, user):
    try:
        res = apply_for_event(event_id, user.token)
    except:
        draw_apply_error(user)
        return

    event = get_event(event_id, user.token)

    if res == 200:
        draw_apply_success(user, event['title'])
    elif res == 409:
        draw_apply_already(user, event['title'])


def draw_apply_success(user, event_name):
    user.send_message(
        f'You have become a part of {event_name}!'
    )


def draw_apply_already(user, event_name):
    user.send_message(
        f'It seems you are already a participant in {event_name}!'
    )


def draw_apply_error(user):
    user.send_message(
        f'We seem to be having some problems \U0001F61F You may have used an invalid event link. If you are sure that everything is in order, please write to my creator @peramor.'
    )
